# Rep: Free

Migliora la lettura degli articoli di [Rep:](https://rep.repubblica.it/) rendendola gratuita.


## Changelog:

### [2.0.2]() - 2019-10-31

* fix cambiamento struttura DOM del 2019-10-31

### [2.0.1]() - 2019-03-15

* fix cambiamento struttura DOM del 2019-03-13

### [2.0.0]() - 2019-28-02

* rimuove il supprto per l'Espresso
* rifattorizza e semplifica il codice
* rimuove il caricamento dei contenuti dal Json esterno
* aggiunge Makefile e build tools
* aggiorna la major version, da ora in poi i fix per il cambiamento del DOM
  aumenteranno solo la build version 

### [1.6]() - 2019-020-27 

* fix cambiamento struttura DOM del 2019-02-27


### [1.5]() - 2019-02-19

* fix cambiamento struttura DOM del 2019-02-19


### [1.4]() - 2019-01-02 

* fix cambiamento struttura DOM 


### [1.3]() - 2018-09-07

* aggiunge il supporto per chrome


### [1.2]() - 2018-08-30 

* carica l'articolo dal json invece che dal testo della pagina,
     dopo che Repubblica ha iniziato a servire il testo troncato.
