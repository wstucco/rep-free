const log = (msg) => console.log(`Rep-Free: ${msg}`);

const findPaywall = (root) => {
  try {

    return root.shadowRoot.querySelector('NEWS-ARTICLE')
      .shadowRoot.querySelector('.amp-doc-host')
      .shadowRoot.querySelector('[subscriptions-section=content-not-granted]');
  } catch (ex) {

    return null;
  }
}

log('Checking ...');

if (window.location.href.indexOf('/pwa/') > -1) {

  const start = -performance.now();

  const interval = setInterval(() => {

    const paywall = findPaywall(document.body.querySelector('news-app'));

    if (paywall !== null) {

      log('Paywall found, displaying content ...');

      paywall.nextSibling.removeAttribute('subscriptions-section');
      paywall.remove();

      const time = ((start + performance.now()) / 1000).toFixed(2);
      log(`done in ${time} sec.`);

      clearInterval(interval);
    }

    if (start + performance.now() >= 5000) {

      log('timeout!');
      clearInterval(interval);
    }

  }, 100);
}