.PHONY: all

all:
	@VERSION=$$(grep -h '"version"' manifest.json | cut -d'"' -f 4); \
		zip -r -FS ../rep-free-"$${VERSION}".zip repfree.js icons/icon-*.png LICENSE manifest.json; \
		echo "Zip file is ready: ../rep-free-$${VERSION}.zip";

	@./node_modules/.bin/web-ext build -o -i Makefile .gitignore package.json yarn.lock node_modules ".*"

ifneq ($(and $(strip $(JWT_USER)),$(strip $(JWT_SECRET))),)
	@./node_modules/.bin/web-ext sign --api-key=${JWT_USER} --api-secret=${JWT_SECRET}
endif



